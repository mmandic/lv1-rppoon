using System;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            Notes note = new Notes();
            Console.WriteLine(note.Author);
            Console.WriteLine(note.Text);
            note = new Notes("Everything is gonna be alright tommorrow!", 2);
            Console.WriteLine(note.Author);
            Console.WriteLine(note.Text);
            note = new Notes("Drink coffee every day!", "Mihaela", 3);
            Console.WriteLine(note.Author);
            Console.WriteLine(note.Text);
            Console.WriteLine(note.ToString());

            TimeNote timeNote = new TimeNote();
            Console.WriteLine(timeNote.ToString());
            
            

        }
    }
}