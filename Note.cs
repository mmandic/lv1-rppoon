using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class Note
    {
        private string text;
        private string author;
        private int importance;

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }

        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }

        public int Importance
        {
            get { return this.importance; }
            set { this.importance = value; }
        }

        public Note()
        {
            this.text = "Wash your hands!";
            this.author = "Scarlett O'Hara";
            this.importance = 5;

        }

        public Note(string text, string author, int importance)
        {
            this.text = text;
            this.author = author;
            this.importance = importance;
        }

        public Note(string text, int importance)
        {
            this.text = text;
            this.author = "Audrey Hepburn";
            this.importance = importance;
        }

        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        public int getImportance() { return this.importance; }

        public void setText(string text) { this.text = text; }
        private void setAuthor(string author) { this.author = author; }
        public void setImportance(int importance) { this.importance = importance; }


        public override string ToString()
        {
            return this.Text + ", " + this.Author + ", " + this.Importance;
        }
    }

}