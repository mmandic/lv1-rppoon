using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class TimeStamp : Note
    {
        private DateTime time;

        public TimeStamp()
        {
            this.time = DateTime.Now;
        }

        public TimeStamp(string text, string author, int importance, DateTime time) : base(text, author, importance)
        {
            this.time = time;
        }

        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

        public override string ToString()
        {
            return base.ToString() + ", " + this.Time;
        }
    }
}